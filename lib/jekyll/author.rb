module JekyllPlugins

	class AuthorPage < Jekyll::Page
		def initialize(site, base, dir, author)
			@site = site
			@base = base
			@dir = dir
			@name = 'index.html'

			self.process(@name)
			self.read_yaml(File.join(base, '_layouts'), 'author.html')

			self.data['title'] = author['name']
			self.data['description'] = author['bio']
			self.data['author'] = author
		end
	end

	class AuthorGenerator < Jekyll::Generator
		safe true

		def generate(site)
			puts "Generating Author Pages"	
			dir = site.config['authors_dir'] || 'authors'
			puts "Authors pages directory: " + dir
			if site.data['authors']
				site.data['authors'].each do |author_key, author|
					author_post = Array.new
					site.posts.docs.each do |post|
						if post.data['author'] == author_key
							post.data['author'] = author
							author_post << post
						end
					end
					author['posts'] = author_post
					author['slug'] = File.join(dir, Jekyll::Utils.slugify(author_key))
					author['url'] = "/" + author['slug']
					site.pages << AuthorPage.new(site, site.source, author['slug'], author)
				end
			end		
		end
	end
end