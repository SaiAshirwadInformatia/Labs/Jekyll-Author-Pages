# Jekyll Author [![build status](https://gitlab.com/SaiLabs/Jekyll-Author-Pages/badges/master/build.svg)](https://gitlab.com/SaiLabs/Jekyll-Author-Pages/commits/master) [![Gem Version](https://badge.fury.io/rb/jekyll-author-pages.svg)](https://badge.fury.io/rb/jekyll-author-pages)

This plugin helps generate Author based pages for Jekyll static site.

### Installation

This plugin is available as a [RubyGem](https://rubygems.org/gems/jekyll-author/)

Add this line to your application's Gemfile:

```
gem 'jekyll-author-pages'
```

And then execute the `bundle install` command to install the gem.

Alternatively, you can also manually install the gem using the following command:

```
$ gem install jekyll-author-pages
```

For Jekyll Users, after the plugin has been installed successfully, add the following lines to your _config.yml in order to tell Jekyll to use the plugin:

```yaml
gems:
  - jekyll-author-pages
```

### Usage

Firstly you need to add data of `authors` into data directory as `data/authors.yml` or `data/authors.json`

#### Sample Data

```yaml
rsakhale:
  name: Rohan Sakhale
  bio: PHP/Java/GitLab fan, Passionate Developer and Loves Teaching
  website: https://rohansakhale.com
  twitter: RohanSakhale
  linkedin: https://in.linkedin.com/in/rsakhale
  github: RohanSakhale
  gitlab: rsakhale
cpatil:
  name: Chaitali Patil
  bio: Crispy PHP developer and UI designer, believer in re-usability
  website: https://cpatil.gitlab.io
  twitter: chaitaalipatil
  linkedin: https://in.linkedin.com/in/chaitali-patil-155397a3
  github: chaitalipatil
  gitlab: cpatil
```

Then you need to add `author.html` layout for creating the view of your author page, this page will have `page.author` variable available for displaying any contents related to this author. Additionally posts written by this author would be available over `page.author.posts`.

For identifying post author you need to mention `author: username` in your **post front matter**

```yaml
---
title: Sample Post
author: rsakhale
---
```

### Contribute

Fork this repository, create your branch, make changes and then issue a pull request.

If you find bugs or have new ideas that you do not want to implement yourself, file a bug report.

### Copyright

Copyright (c) 2016 Sai Ashirwad Informatia

License: [MIT](LICENSE.md)