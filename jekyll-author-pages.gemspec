# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll/plugin_version'

Gem::Specification.new do |spec|
  spec.name        = 'jekyll-author-pages'
  spec.version     = JekyllPlugins::Author::VERSION
  spec.summary     = "Generate Author pages for Jekyll based sites"
  spec.description = "Generate Author pages for Jekyll based sites"
  spec.authors     = ["Rohan Sakhale"]
  spec.email       = 'rs@saiashirwad.com'
  spec.files         = [*Dir["lib/**/*.rb"], *Dir["_layouts/*.html"], "README.md", "LICENSE.md"]
  spec.require_paths = ['lib']
  spec.homepage    = 'https://gitlab.com/SaiLabs/Jekyll-Author-Pages'
  spec.license     = 'MIT'

   # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem push.'
  end

  spec.add_runtime_dependency 'jekyll', '~> 3.3.0'
  spec.add_development_dependency 'bundler', '~> 1.10'
end